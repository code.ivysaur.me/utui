# utui

![](https://img.shields.io/badge/written%20in-PHP-blue)

A multi-user web interface for uTorrent.

This is a simple multi-user PHP interface to the uTorrent WebUI. It makes minimal use of javascript, in order to maximise performance and to be accessible from any weak browser (mobile phones, WinCE etc).

Tags: torrent

## See Also

Original thread: http://forum.utorrent.com/viewtopic.php?pid=568654


## Download

- [⬇️ utui r1.rar](dist-archive/utui%20r1.rar) *(6.56 KiB)*
